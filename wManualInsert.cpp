#include "wManualInsert.hpp"

#include <QDebug>

wManualInsert::wManualInsert(QDialog* parent)
    : QDialog(parent), ui(new Ui::wManualInsert) {
  ui->setupUi(this);
  valid = false;
  connect(ui->buttonBox, SIGNAL(accepted()), this,
          SLOT(on_buttonBox_accepted()));
  connect(ui->buttonBox, SIGNAL(rejected()), this,
          SLOT(on_buttonBox_rejected()));
}

wManualInsert::~wManualInsert() {}

void wManualInsert::on_buttonBox_accepted() {
  valid = true;
  accept();
}

void wManualInsert::on_buttonBox_rejected() {
  reject();
}

bool wManualInsert::validData() const {
  return valid && getCodFis() != "" && getNome() != "" && getCognome() != "";
}