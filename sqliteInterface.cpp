#include "sqliteInterface.hpp"

#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>

SqliteInterface::SqliteInterface(const QString& path) {
  m_db = QSqlDatabase::addDatabase("QSQLITE");
  m_db.setDatabaseName(path);

  if (!m_db.open()) {
    qDebug() << "Error: connection with database fail";
  } else {
    qDebug() << "Database: connection ok";
  }
}

bool SqliteInterface::addVisitor(const QString& codfis,
                                 const QString& anagrafica) {
  bool success = false;
  QSqlQuery query;
  query.prepare(
      "INSERT INTO presenze (checkin, codfis, anagrafica) VALUES "
      "(DATETIME('now'), :codfis, "
      ":anagrafica)");
  query.bindValue(":codfis", codfis);
  query.bindValue(":anagrafica", anagrafica);
  if (query.exec()) {
    success = true;
  } else {
    qDebug() << __FUNCTION__ << " error:  " << query.lastError();
  }
  return success;
}

bool SqliteInterface::cleanup() {
  bool success = false;
  QSqlQuery query;
  query.prepare(
      "DELETE FROM presenze WHERE checkin < DATETIME('NOW', '-15 days')");
  if (query.exec()) {
    success = true;
  } else {
    qDebug() << __FUNCTION__ << " error:  " << query.lastError();
  }
  return success;
}

int SqliteInterface::count() {
  QSqlQuery query;
  query.prepare("SELECT COUNT(*) from presenze");
  if (query.exec()) {
    query.first();
    return query.value(0).toInt();
  } else {
    qDebug() << __FUNCTION__ << " error:  " << query.lastError();
    return 0;
  }
}