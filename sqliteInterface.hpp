#ifndef __SQLITEINTERFACE_HPP__
#define __SQLITEINTERFACE_HPP__

#include <QSqlDatabase>
#include <QString>

class SqliteInterface {
 public:
  SqliteInterface(const QString& path);
  bool cleanup();
  bool addVisitor(const QString& codfis, const QString& anagrafica);
  int count();

 private:
  QSqlDatabase m_db;
};

#endif