#ifndef __WMANUALINSERT_HPP__
#define __WMANUALINSERT_HPP__

#include <QDialog>

#include "ui_wManualInsert.h"

namespace Ui {
class wManualInsert;
}

class wManualInsert : public QDialog {
  Q_OBJECT

 public:
  explicit wManualInsert(QDialog* parent = 0);
  ~wManualInsert();

 private slots:
  // GUI Events
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();

 private:
  Ui::wManualInsert* ui;
  bool valid;

  // Fields getters
 public:
  QString getNome() const { return ui->lineNome->text().trimmed(); };
  QString getCognome() const { return ui->lineCognome->text().trimmed(); };
  QString getCodFis() const { return ui->lineCodFis->text().trimmed(); };
  bool validData() const;
};

#endif