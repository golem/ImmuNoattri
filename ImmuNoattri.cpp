#include "ImmuNoattri.hpp"

#include <CNS.h>  // https://git.golem.linux.it/golem/cnsparser

#include <QDebug>

#include "ui_ImmuNoattri.h"

bool decodeCodfis(const QString& input, QString& codfis,
                  Anagrafica& anagrafica) {
    QStringList list = input.split('_');
    if (list.size() != 4) {
        return false;
    }

    codfis = list.at(1);
    if (codfis.length() != 16) {
        return false;
    }

    QString anagrafica_magnetica = list.at(2);

    QStringList anagrafica_list = anagrafica_magnetica.split("  ");
    if (anagrafica_list.size() != 2) {
        return false;
    }
    anagrafica.cognome = anagrafica_list.at(0);
    anagrafica.nome = anagrafica_list.at(1);

    return true;
}

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      sqliteInterface("presenze.db") {
    ui->setupUi(this);

    qDebug() << "Pulisco db";
    sqliteInterface.cleanup();

    connect(&lineTessera_timer, &QTimer::timeout, this,
            &MainWindow::on_lineTessera_timer_timeout);

    connect(ui->statusbar, &QStatusBar::messageChanged, this,
            &MainWindow::on_statusbar_changed);

    int registrati = sqliteInterface.count();
    ui->lineRegistrati->setText(QString::number(registrati));
    on_statusbar_changed();

    connect(&smartcard_check_timer, &QTimer::timeout, this,
            &MainWindow::on_smartcard_check);
    smartcard_check_timer.start(std::chrono::seconds(2));
}

void MainWindow::on_statusbar_changed() {
    if ((ui->statusbar->currentMessage()).length() == 0) {
        ui->statusbar->setStyleSheet("background-color: auto;");
        ui->statusbar->showMessage(
            "Passare la tessera magnetica sul lettore, inserire una smartcard "
            "nel lettore, o effettuare un inserimento manuale.",
            0);
    }
}

MainWindow::~MainWindow() {}

void MainWindow::on_actionAbout_triggered() {
    qDebug() << "An about string...";
}

void MainWindow::on_lineTessera_textEdited(const QString& text) {
    // The magnetic reader has three newline characters
    lineTessera_timer.start(std::chrono::milliseconds(500));
}

void MainWindow::on_lineTessera_timer_timeout() {
    lineTessera_timer.stop();

    QString codfis;
    Anagrafica anagrafica;
    // Validating input
    if (decodeCodfis(ui->lineTessera->text(), codfis, anagrafica)) {
        addVisitor(codfis, anagrafica);
    } else {
        ui->statusbar->showMessage("Tessera non valida, riprovare!",
                                   MESSAGE_TIMEOUT);
        ui->statusbar->setStyleSheet("background-color: red;");
    }

    ui->lineTessera->setText("");
}

/**
 * @brief Callback pulsante per inserimento manuale dei dati
 *
 */
void MainWindow::on_pushMano_clicked() {
    wmanualInsert = std::make_unique<wManualInsert>();
    wmanualInsert->exec();
    if (wmanualInsert->validData()) {
        QString codfis = wmanualInsert->getCodFis();
        Anagrafica anagrafica;
        anagrafica.cognome = wmanualInsert->getCognome();
        anagrafica.nome = wmanualInsert->getNome();
        addVisitor(codfis, anagrafica);
    }
    wmanualInsert.release();
}

void MainWindow::addVisitor(QString& codfis, Anagrafica& anagrafica) {
    sqliteInterface.addVisitor(
        codfis, QString("%1 %2").arg(anagrafica.cognome).arg(anagrafica.nome));
    int registrati = ui->lineRegistrati->text().toInt();
    registrati++;
    ui->lineRegistrati->setText(QString::number(registrati));
    ui->statusbar->showMessage("Inserimento effettuato correttamente!",
                               MESSAGE_TIMEOUT);
    ui->statusbar->setStyleSheet("background-color: green;");
    ui->labelNome->setText(QString("Ciao %1!").arg(anagrafica.nome));
}

void MainWindow::closeEvent(QCloseEvent* event) {}

void MainWindow::on_smartcard_check() {
    static QString last_codice_fiscale;
    CNS cns;

    try {
        cns.fromSmartCard();

        if (cns.getCodiceFiscale() == last_codice_fiscale) return;

        last_codice_fiscale = cns.getCodiceFiscale();

        QString codfis = cns.getCodiceFiscale();
        Anagrafica anagrafica;
        anagrafica.cognome = cns.getCognome();
        anagrafica.nome = cns.getNome();

        addVisitor(codfis, anagrafica);
    } catch (Ex e) {
        // no valid smartcard
    }
}