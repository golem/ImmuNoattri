# ImmuNoattri
A terrible access monitor software based based on EU healthcare smartcard.

## How it works
To record close contacts in our [Officina Informatica](https://wiki.golem.linux.it/Officina_Informatica), as initially mandated by regional law 2021-05-27/60 [Regione Toscana](https://www.regione.toscana.it/), in order to prevent diffusion of COVID-19, just remember to swipe your healthcare card in Lampredotto's totem, as soon as you enter the building.
Your name will be recorded, in order to inform health authorities in case of COVID-19 contact.
Your name will also be automatically deleted after 15 days.

## Requirements
* libqt5sql5-sqlite
* sqlite3
* [cnsparser](https://git.golem.linux.it/golem/cnsparser)

## Database
```
CREATE TABLE presenze (ID INTEGER PRIMARY KEY AUTOINCREMENT, checkin TEXT, codfis TEXT, anagrafica TEXT);
```
## Magnetic card format
```_CGNNMM00A41D403C_COGNOME  NOME1 NOME2_```
