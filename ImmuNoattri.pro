QT       = core gui
QT      += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImmuNoattri
TEMPLATE = app

LIBS += -lcnsparser

CONFIG += link_pkgconfig
PKGCONFIG += libpcsclite

SOURCES += main.cpp \
           ImmuNoattri.cpp \
           sqliteInterface.cpp \
           wManualInsert.cpp

HEADERS  += ImmuNoattri.hpp \
            sqliteInterface.hpp \
            wManualInsert.hpp

FORMS    += ImmuNoattri.ui \
            wManualInsert.ui

QMAKE_CXXFLAGS_RELEASE = $$QMAKE_CXXFLAGS_RELEASE_WITH_DEBUGINFO
QMAKE_CFLAGS_RELEASE = $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
QMAKE_LFLAGS_RELEASE = $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO
