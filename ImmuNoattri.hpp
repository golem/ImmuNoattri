#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCloseEvent>
#include <QFile>
#include <QMainWindow>
#include <QTimer>
#include <memory>

#include "Anagrafica.hpp"
#include "sqliteInterface.hpp"
#include "wManualInsert.hpp"

const int MESSAGE_TIMEOUT = 15e3;  //  15 s

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

   public:
    /**
     * @brief MainWindow        Constructor
     * @param parent            Parent Handle
     */
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

   private slots:
    // GUI Events
    void on_pushMano_clicked();
    void on_actionAbout_triggered();
    void on_lineTessera_textEdited(const QString& text);
    void on_lineTessera_timer_timeout();
    void on_statusbar_changed();
    void on_smartcard_check();

   private:
    Ui::MainWindow* ui;
    std::unique_ptr<wManualInsert> wmanualInsert;
    QFile* csv_file;
    QTimer lineTessera_timer;
    QTimer smartcard_check_timer;
    SqliteInterface sqliteInterface;

    void closeEvent(QCloseEvent* event);
    void addVisitor(QString& codfis, Anagrafica& anagrafica);
};

#endif